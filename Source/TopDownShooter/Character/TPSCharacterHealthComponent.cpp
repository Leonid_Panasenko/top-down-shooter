// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSCharacterHealthComponent.h"

void UTPSCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{
			//FX
			UE_LOG(LogTemp, Warning, TEXT("void UTPSCharacterHealthComponent::ChangeHealthValue - Shield < 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTPSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShield,
			this,
			&UTPSCharacterHealthComponent::CoolDownShieldEnd,
			CoolDownShieldRecoveryTime,
			false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRate);
	}

	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UTPSCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRate,
			this,
			&UTPSCharacterHealthComponent::RecoveryShield,
			ShieldRecoveryRate,
			true);
	}
}

void UTPSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield + ShieldRecoveryValue;
	
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRate);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}
