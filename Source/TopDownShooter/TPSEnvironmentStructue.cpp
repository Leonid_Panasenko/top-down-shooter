// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSEnvironmentStructue.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
ATPSEnvironmentStructue::ATPSEnvironmentStructue()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATPSEnvironmentStructue::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPSEnvironmentStructue::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATPSEnvironmentStructue::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}

	return Result;
}

TArray<UTPS_StateEffect*> ATPSEnvironmentStructue::GetAllCurrentEffects()
{
	return Effects;
}

void ATPSEnvironmentStructue::RemoveEffect(UTPS_StateEffect* toRemoveEffect)
{
	Effects.Remove(toRemoveEffect);
}

void ATPSEnvironmentStructue::AddEffect(UTPS_StateEffect* newEffect)
{
	Effects.Add(newEffect);
}

void ATPSEnvironmentStructue::GetAttachPlace(FName& NameBone, FVector& Offset)
{
	NameBone = NameBoneToAttach;
	Offset = AttachOffset;
}
