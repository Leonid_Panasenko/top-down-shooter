// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/TPS_IGameActor.h"
#include "TPSEnvironmentStructue.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATPSEnvironmentStructue : public AActor, public ITPS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATPSEnvironmentStructue();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName NameBoneToAttach;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector AttachOffset;

	//Interface
	EPhysicalSurface GetSurfaceType() override;
	TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTPS_StateEffect* toRemoveEffect) override;
	void AddEffect(UTPS_StateEffect* newEffect) override;
	void GetAttachPlace(FName& NameBone, FVector& Offset) override;
	//End Interface

	//Effects
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	TArray<UTPS_StateEffect*> Effects;
};


